from collections import deque

import cv2
import numpy as np 
import torch
import argparse

from src.config import *

canvas = np.zeros((480, 640, 3), dtype=np.uint8)
points = deque(maxlen=512)
is_drawing, is_shown = False, False
point_x , point_y = None , None

def get_args():
    parser = argparse.ArgumentParser(
        """Implementation of Google's Quick Draw Project (https://quickdraw.withgoogle.com/#)""")
    parser.add_argument("-c", "--color", type=str, choices=["green", "blue", "red"], default="green",
                        help="Color which could be captured by camera and seen as pointer")
    parser.add_argument("-a", "--area", type=int, default=3000, help="Minimum area of captured object")
    parser.add_argument("-d", "--display", type=int, default=3, help="How long is prediction shown in second(s)")
    parser.add_argument("-s", "--canvas", type=bool, default=False, help="Display black & white canvas")
    args = parser.parse_args()
    return args

def point_drawing(event, x, y, flags, param):
    global point_x, point_y, is_drawing, is_shown, canvas, points
    if event == cv2.EVENT_LBUTTONDOWN:
        is_drawing = True
        point_x, point_y = x, y
        points.appendleft((x, y))
    elif event == cv2.EVENT_MOUSEMOVE:
        if is_drawing:
            points.appendleft((x, y))
            cv2.line(canvas, (point_x, point_y), (x, y), color=(255,255,255), thickness=5)
            point_x, point_y = x, y
    elif event == cv2.EVENT_LBUTTONUP:
        is_drawing = False
        points.appendleft((x, y))
        cv2.line(canvas, (point_x, point_y), (x, y), color=(255,255,255), thickness=5)
        if is_shown:
            points = deque(maxlen=512)
            canvas = np.zeros((480, 640, 3), dtype=np.uint8)
        is_shown = False

def main(opt):
    global is_drawing, is_shown, canvas, points
    # Init configuration
    cv2.namedWindow('Drawing App')
    cv2.setMouseCallback('Drawing App', point_drawing)
    predicted_class = None

    # Load model
    if torch.cuda.is_available():
        model = torch.load("trained_models/whole_model_quickdraw")
    else:
        model = torch.load("trained_models/whole_model_quickdraw", map_location=lambda storage, loc: storage)
    model.eval()

    while True:
        k = cv2.waitKey(10)
        if k == ord('q'):
            break
        if not is_drawing and not is_shown:
            if len(points):
                canvas_gs = cv2.cvtColor(canvas, cv2.COLOR_BGR2GRAY)
                # Blur image
                median = cv2.medianBlur(canvas_gs, 9)
                gaussian = cv2.GaussianBlur(median, (5, 5), 0)
                # Otsu's thresholding
                _, thresh = cv2.threshold(gaussian, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
                contour_gs, _ = cv2.findContours(thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
                if len(contour_gs):
                    contour = sorted(contour_gs, key=cv2.contourArea, reverse=True)[0]
                    # Check if the largest contour satisfy the condition of minimum area
                    if cv2.contourArea(contour) > opt.area:
                        x, y, w, h = cv2.boundingRect(contour)
                        image = canvas_gs[y:y + h, x:x + w]
                        image = cv2.resize(image, (28, 28))
                        image = np.array(image, dtype=np.float32)[None, None, :, :]
                        image = torch.from_numpy(image)
                        image = image.cuda()
                        logits = model(image)
                        predicted_class = torch.argmax(logits[0])
                        is_shown = True
                    else:
                        print("The object drawn is too small. Please draw a bigger one!")
                        points = deque(maxlen=512)
                        canvas = np.zeros((480, 640, 3), dtype=np.uint8)
        if is_shown:
            # print(predicted_class.item())
            print(f'You are drawing: {CLASSES[predicted_class]}')
            """
            if predicted_class == current_ques:
                # Generate new quest
                current_ques = classes_list.pop()
                print(f'Draw a/an {current_ques}')
            """
            points = deque(maxlen=512)
            canvas = np.zeros((480, 640, 3), dtype=np.uint8)
            is_shown = False
                    
        cv2.imshow('Drawing App', canvas)
        if opt.canvas:
            cv2.imshow("Canvas", canvas) 
    cv2.destroyAllWindows()

if __name__ == "__main__":
    opt = get_args()
    main(opt)