from datetime import datetime

class FPSCounter:
    def __init__(self):
        self._start_time = None
        self._num_occurrences = 0

    def start(self):
        self._start_time = datetime.now()
        return self
    
    def increment(self):
        self._num_occurrences += 1

    def get_frame_per_second(self):
        end = datetime.now()
        elapsed_time = (end - self._start_time).total_seconds()
        return self._num_occurrences//elapsed_time